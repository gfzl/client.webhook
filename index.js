var http = require('http');
var exec = require('shelljs').exec;

const PORT = 6603;

console.log('listening port at: ' + PORT);

const cmds = {
    'service/users': 'cd /mnt/authing/users && git pull',
    'service/emails': 'cd /mnt/authing/emails && git pull',
    'service/pay': 'cd /mnt/authing/pay && git pull',
    'service/oauth': 'cd /mnt/authing/oauth && git pull',
    client: 'cd /mnt/authing/client && git pull'
};

console.log(cmds);

var deployServer = http.createServer(function(request, response) {
    // var inCMDs = false,
    //     cmd = '';

    // for (var key in cmds) {
    //     if (key == request.url) {
    //         inCMDs = true;
    //         cmd = cmds[key];
    //         break;
    //     }
    // }

    // if (inCMDs) {
    //     exec(cmd, function(err, out, code) {
    //         if (err instanceof Error) {
    //             response.writeHead(500);
    //             response.end('Server Internal Error.');
    //             throw err;
    //         }
    //         process.stderr.write(err.toString());
    //         process.stdout.write(out.toString());
    //         response.writeHead(200);
    //         response.end('Deploy Done.');
    //     });
    // } else {
    //     response.writeHead(404);
    //     response.end('Invalid Deploy Request');
    // }
    console.log(request)
});

deployServer.listen(PORT);
